package com.spt.engine.entity.general;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import org.hibernate.annotations.Formula;
import org.springframework.beans.factory.annotation.Required;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Data
@Entity
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class User {
	
	private @Id @GeneratedValue @Column(name="user_id") Long id;
	private @Version @JsonIgnore Long version;
	private String createdBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	
	
	@NonNull private String username;
	@NonNull private String accessToken;
	@NonNull private String firstName;
	@NonNull private String lastName;
	
	@Formula("CONCAT(first_name,' ',last_name)")
	private String fullName;
	
	private String description;
	
	
	private Boolean accountNonExpired;
	private Boolean credentialsNonExpired;
	private Boolean accountNonLocked;
	private String  email;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "userRole", joinColumns = {
			@JoinColumn(name = "user_id", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "role_id",nullable = false, updatable = false) })
    private Set<Role> roles = new HashSet<Role>();
	
	public Set<Role> getAuthorities(){
		return this.roles;
	}
}
