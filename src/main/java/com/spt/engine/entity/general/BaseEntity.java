package com.spt.engine.entity.general;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class BaseEntity  {
	
	private @Id @GeneratedValue Long id;
	private @Version @JsonIgnore Long version;
	
	
	private String createdBy;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Timestamp createdDate;
	private String updateBy;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Timestamp updateDate;
	
	
}
