package com.spt.engine.entity.general;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class MasterDataDetail extends BaseEntity{
	
	private String code;
	private String name;
	private String description;
	private Boolean flagActive;
	private String orgCode;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "masterdata")
    private MasterData masterdata;
	
	private String variable1;
	private String variable2;
	private String variable3;
	private String variable4;
	private String variable5;
	private String variable6;
	private String variable7;
	private String variable8;
	private String variable9;
	private String variable10;
	private String variable11;
	private String variable12;
	private String variable13;
	private String variable14;
	private String variable15;
	private String variable16;
	private String variable17;
	private String variable18;
	private String variable19;
	private String variable20;
	private String variable21;
	private String variable22;
	private String variable23;
	private String variable24;
	private String variable25;
	private String variable26;
	private String variable27;
	private String variable28;
	private String variable29;
	private String variable30;
	private String variable41;
	private String variable42;
	private String variable43;
	private String variable44;
	private String variable45;
	private String variable46;
	private String variable47;
	private String variable48;
	private String variable49;
	private String variable50;
}
