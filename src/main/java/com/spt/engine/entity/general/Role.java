package com.spt.engine.entity.general;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Data
@Entity
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Role {
	
	private @Id @GeneratedValue @Column(name="role_id") Long id;
	private @Version @JsonIgnore Long version;
	
	
	private String createdBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss") Timestamp createdDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	
	@Formula("CONCAT('ROLE_',role_name)")
	private String role;
	private @Column(unique=true) String roleName;
	private Boolean flagActive;
	
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    private Set<User> user = new HashSet<User>();
	
}
