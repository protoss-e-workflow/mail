package com.spt.engine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobEngineApplication.class, args);
	}
}
