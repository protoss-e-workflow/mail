package com.spt.engine.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;


@Service
public class RestService extends AbstractEngineService {

	static final Logger LOGGER = LoggerFactory.getLogger(RestService.class);
	

	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		// TODO Auto-generated method stub
		super.restTemplate = restTemplate;
	}


	
	public ResponseEntity<String> callJobNumber(String jobNumber) {
		StringBuilder content = new StringBuilder();
		content.append("Dear K.Kae<br /> ");
		content.append("Could you please find the pending payment from you as below and nesting to us as as soon as you can.<br />");
		content.append("if you have any inquiries,please let us know.<br /><br />");
		
		content.append("<table width='1200' height='178' cellpadding='0' cellspacing='0'>");
		content.append("  <tr bordercolor='#000000' bgcolor='#99FF99'>");
		content.append("    <th width='93' style='border: 1px solid black;' ><div align='center'>Customer ID</div></th>");
		content.append("    <th width='268' style='border: 1px solid black;' ><div align='center'>Customer Name </div></th>");
		content.append("    <th width='92' style='border: 1px solid black;' ><div align='center'>Item ID </div></th>");
		content.append("    <th width='97' style='border: 1px solid black;' ><div align='center'>Item Date </div></th>");
		content.append("    <th width='100' bgcolor='#999999'  style='border: 1px solid black;' ><div align='center'>Item Due Date </div></th>");
		content.append("    <th width='98' bgcolor='#999999'  style='border: 1px solid black;' ><div align='center'>Days Overdue </div></th>");
		content.append("    <th width='139' style='border: 1px solid black;' ><div align='center'>Item balance w/VAT in Base Cur </div></th>");
		content.append("    <th width='97' style='border: 1px solid black;' ><div align='center'>Service Month </div></th>");
		content.append("    <th width='99' style='border: 1px solid black;' ><div align='center'>Activity ID </div></th>");
		content.append("  </tr>");
		content.append("  <tr bordercolor='#000000'>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >21202320</div></td>");
		content.append("    <td style='border: 1px solid black;' >Justice Honda Automobile Co.,Ltd.</td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >BTR0034224</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >5/9/2016</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >5/10/2016</div></td>");
		content.append("    <td style='border: 1px solid black;color:red;' ><div align='right' >134</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >8,560.00</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >Sep'16</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >2016SR00227</div></td>");
		content.append("  </tr>");
		content.append("  <tr bordercolor='#000000'>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >21202320</div></td>");
		content.append("    <td style='border: 1px solid black;' >Justice Honda Automobile Co.,Ltd.</td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >BTR0034224</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >5/9/2016</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >5/10/2016</div></td>");
		content.append("    <td style='border: 1px solid black;color:red;' ><div align='right' >134</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >8,560.00</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >Sep'16</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >2016SR00227</div></td>");
		content.append("  </tr>");
		content.append("  <tr bordercolor='#000000'>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >21202320</div></td>");
		content.append("    <td style='border: 1px solid black;' >Justice Honda Automobile Co.,Ltd.</td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >BTR0034224</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >5/10/2016</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >4/11/2016</div></td>");
		content.append("    <td style='border: 1px solid black;color:red;' ><div align='right' >104</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >8,560.00</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >Oct'16</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >2016SR00227</div></td>");
		content.append("  </tr>");
		content.append("  <tr bordercolor='#000000'>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >21202320</div></td>");
		content.append("    <td style='border: 1px solid black;' >Justice Honda Automobile Co.,Ltd.</td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >BTR0034224</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >5/11/2016</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >5/12/2016</div></td>");
		content.append("    <td style='border: 1px solid black;color:red;' ><div align='right' >73</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >8,560.00</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >Nov'16</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >2016SR00227</div></td>");
		content.append("  </tr>");
		content.append("  <tr bordercolor='#000000'>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >21202320</div></td>");
		content.append("    <td style='border: 1px solid black;' >Justice Honda Automobile Co.,Ltd.</td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >BTR0034224</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >5/12/2016</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >4/1/2017</div></td>");
		content.append("    <td style='border: 1px solid black;color:red;' ><div align='right' >43</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='right' >8,560.00</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >Dec'16</div></td>");
		content.append("    <td style='border: 1px solid black;' ><div align='center' >2016SR00227</div></td>");
		content.append("  </tr>");
		content.append("  <tr bordercolor='#000000'>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("  </tr>");
		content.append("  <tr>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td><strong>Netting</strong></td>");
		content.append("    <td>51,360.00</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("    <td>&nbsp;</td>");
		content.append("  </tr>");
		content.append("</table>");
		
		content.append("Thank you.");
		
		// TODO Auto-generated method stub
		String url = "/email/send";
		Map<String, String[]> parameterMap = new HashMap<String, String[]>();
		parameterMap.put("from",        new String[]{"Mr.Suriya Eiamerb"});
		parameterMap.put("fromAddress", new String[]{"suriya_e@softsquaregroup.com"});
		parameterMap.put("addressTo",   new String[]{"suriya_e@softsquaregroup.com"});
		parameterMap.put("subject",     new String[]{"Justice Honda outstanding notification."});
		parameterMap.put("contentText", new String[]{content.toString()});
		return  postWithMapParameter(parameterMap,HttpMethod.POST,url,new RestTemplate ());
		//return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url,new RestTemplate ());
	}
}
