package com.spt.engine;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import com.spt.engine.entity.general.MasterData;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.entity.general.Role;
import com.spt.engine.entity.general.User;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    }
}
