package com.spt.engine.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;


@Component
public class PaidFromBankBBLTasks {
	static final Logger LOGGER = LoggerFactory.getLogger(PaidFromBankBBLTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    @Autowired
    RestTemplate restTemplate;
    
    @Value("${Url.paidFromBankBBL}")
	String urlRest ;


	/* BBL Job ==>  every Day [ 10.27 15.27 17.27 Wait for FTP Server 3=10 Minute]   */
	@Scheduled(cron = "0 50 10,15,17 * * *")
    public void reportCurrentTime() {
    	LOGGER.info("***************************************");
    	LOGGER.info("The time is now {}" , dateFormat.format(new Date()));
    	LOGGER.info("Start paidFromBankBBL job");
    	
    	restTemplate = new RestTemplate();


		LOGGER.info("======= URL TO SEND ==== {}",urlRest);

    	String resultString  = restTemplate.getForObject(urlRest, String.class);
		LOGGER.info("Complete paidFromBankBBL job");
    	LOGGER.info("***************************************");
    }
}
