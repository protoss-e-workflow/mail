package com.spt.engine.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;


@Component
public class ClearAdvanceOutStandingTasks {
	static final Logger LOGGER = LoggerFactory.getLogger(ClearAdvanceOutStandingTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    @Autowired
    RestTemplate restTemplate;
    
    @Value("${Url.clearAdvanceOutStanding}")
	String urlRest ;

	@Value("${Url.clearAdvanceDataNotStable}")
	String urlClearAdvanceDataNotStable ;


    @Scheduled(cron = "0 0 4 * * *")
    public void reportCurrentTime() {
    	LOGGER.info("***************************************");
    	LOGGER.info("The time is now {}" , dateFormat.format(new Date()));
    	LOGGER.info("Start Clear Advance OutStanding Job");
    	
    	restTemplate = new RestTemplate();


		LOGGER.info("======= URL TO SEND ==== {}",urlRest);

    	String resultString  = restTemplate.getForObject(urlRest, String.class);

    	LOGGER.info("***************************************");
    }

	@Scheduled(cron = "0 30 6 * * *")
	public void clearAdvanceDataNotStable() {
		LOGGER.info("***************************************");
		LOGGER.info("The time is now {}" , dateFormat.format(new Date()));
		LOGGER.info("Start Clear Advance Data Not Stable Job");

		restTemplate = new RestTemplate();


		LOGGER.info("======= URL TO SEND ==== {}",urlClearAdvanceDataNotStable);

		String resultString  = restTemplate.getForObject(urlClearAdvanceDataNotStable, String.class);

		LOGGER.info("***************************************");
	}
}
