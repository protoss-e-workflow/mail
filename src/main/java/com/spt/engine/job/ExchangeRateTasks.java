package com.spt.engine.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.spt.engine.service.RestService;


@Component
public class ExchangeRateTasks {
	static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRateTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    @Autowired
    RestTemplate restTemplate;
    
    @Value("${Url.exchangeRate}")
	String urlRest ;

    //@Scheduled(cron = "0 35 8,9,12,18 * * *")

    @Scheduled(cron = "0 30 19 * * *")    // Every 19.30
//    @Scheduled(cron = "5 * * * * *")    // Every 1 Min
    public void reportCurrentTime() {
    	LOGGER.info("***************************************");
    	LOGGER.info("The time is now {}" , dateFormat.format(new Date()));
    	LOGGER.info("Start exchangeRate job");
    	
    	restTemplate = new RestTemplate();


		LOGGER.info("======= URL TO SEND ==== {}",urlRest);

    	String resultString  = restTemplate.getForObject(urlRest, String.class);

    	LOGGER.info("***************************************");
    }
}
