package com.spt.engine.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class MonthlyPhoneBillTasks {

    static final Logger LOGGER = LoggerFactory.getLogger(MonthlyPhoneBillTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    @Autowired
    RestTemplate restTemplate;

    @Value("${Url.resetMonthlyPhoneBill}")
    String urlRest ;

    @Scheduled(cron = "0 0 0 1 1/1 *")
    public void reportCurrentTime() {
        LOGGER.info("***************************************");
        LOGGER.info("The time is now {}" , dateFormat.format(new Date()));
        LOGGER.info("Start Reset Monthly Phone Bill");

        restTemplate = new RestTemplate();

        LOGGER.info("======= URL TO SEND ==== {}",urlRest);

        String resultString  = restTemplate.getForObject(urlRest, String.class);

        LOGGER.info("Result   :   {}",resultString);

        LOGGER.info("***************************************");
    }
}
