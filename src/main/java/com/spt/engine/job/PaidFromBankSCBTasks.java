package com.spt.engine.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;


@Component
public class PaidFromBankSCBTasks {
	static final Logger LOGGER = LoggerFactory.getLogger(PaidFromBankSCBTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    @Autowired
    RestTemplate restTemplate;
    
    @Value("${Url.paidFromBankSCB}")
	String urlRest ;

	/*SCB Job ==> every day [ 10.27 14.27 17.27 22.27 ]   */
    @Scheduled(cron = "0 50 10,14,17,22 * * *")
    public void reportCurrentTime() {
    	LOGGER.info("***************************************");
    	LOGGER.info("The time is now {}" , dateFormat.format(new Date()));
    	LOGGER.info("Start paidFromBankSCB job");
    	
    	restTemplate = new RestTemplate();


		LOGGER.info("======= URL TO SEND ==== {}",urlRest);

    	String resultString  = restTemplate.getForObject(urlRest, String.class);
		LOGGER.info("Complete paidFromBankSCB job");
    	LOGGER.info("***************************************");
    }
}
