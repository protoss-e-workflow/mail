package com.spt.engine.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class NotiAdvanceOutstandingTasks {

    static final Logger LOGGER = LoggerFactory.getLogger(NotiAdvanceOutstandingTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    RestTemplate restTemplate;

    @Value("${Url.notiAdvanceOutstanding}")
    String urlRest ;


    @Scheduled(cron = "0 0 8 * * *")
    public void reportCurrentTime() {
        LOGGER.info("***************************************");
        LOGGER.info("The time is now {}" , dateFormat.format(new Date()));
        LOGGER.info("Start Noti Advance Outstanding Job");

        restTemplate = new RestTemplate();


        LOGGER.info("======= URL TO SEND ==== {}",urlRest);

        String resultString  = restTemplate.getForObject(urlRest, String.class);

        LOGGER.info("***************************************");
    }
}
